package rasterdata

import io.vavr.collection.Vector
import rasterdata.interfaces.RasterImage
import java.util.*

/**
 * Created by stepanzalis on 15.11.17.
 */

open class RasterImageImmu<T> : RasterImage<T> {

    private var width: Int = 0
    private var height: Int = 0
    private var image: Vector<T>

    private constructor(image: Vector<T>, width: Int, height: Int) {
        this.image = image
        this.width = width
        this.height = height
    }

    override fun getPixel(c: Int, r: Int): Optional<T> {
        return if (c in 0..(width - 1) && 0 <= r && r < height)
            Optional.of(image.get(r * width + c))
        else Optional.empty()
    }

    override fun withPixel(c: Int, r: Int, value: T): RasterImage<T> {
        return if (c >= 0 && c < getWidth() && r >= 0 && r < getHeight()) {
            RasterImageImmu(image.update(r * width + c, value), width, height)
        } else this
    }

    override fun cleared(value: T): RasterImage<T> = cleared(width, height, value)

    override fun getWidth(): Int = width

    override fun getHeight(): Int = height

    companion object {

        fun <T> cleared(width: Int, height: Int, value: T): RasterImageImmu<T> =
                RasterImageImmu(Vector.fill(width * height, { value }), width, height)
    }
}
