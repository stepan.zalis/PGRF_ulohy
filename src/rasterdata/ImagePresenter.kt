package rasterdata

import rasterdata.interfaces.RasterImage

interface ImagePresenter<SomePixelType, DeviceType> {

    fun present(
            image: RasterImage<SomePixelType>,
            device: DeviceType): DeviceType
}
