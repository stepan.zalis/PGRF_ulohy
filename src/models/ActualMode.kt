package models

/**
 * Created by stepanzalis on 23.12.17.
 */

enum class ActualMode {

    LINE, CIRCLE
}
