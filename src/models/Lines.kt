package models

import rasterdata.interfaces.RasterImage
import rasterizationops.LineRendererNaive
import java.awt.*
import java.util.ArrayList

/**
 * Created by stepanzalis on 22.12.17.
 */

open class Lines {

    private var points = ArrayList<Point>()
    private val lineRenderer: LineRendererNaive<Int> = LineRendererNaive()

    fun addPoint(point: Point) {
        points.add(point)
    }

    fun changeLastPoint(point: Point) {
        points[points.lastIndex] = point
    }

    fun drawLines(r: RasterImage<Int>): RasterImage<Int> {

        var rasterImage = r

        for (i in 0..points.size - 2) {
            val pointA = points[i]
            val pointB = points[i + 1]

            rasterImage = draw(rasterImage, pointA, pointB, Color.BLACK.rgb)
        }

        return draw(rasterImage, points[0], points[points.lastIndex], Color.BLACK.rgb)

    }

    /**
     * @param pointA start point
     * @param pointB end point
     */
    fun draw(rasterImage: RasterImage<Int>, pointA: Point, pointB: Point, color: Int): RasterImage<Int> {
        return lineRenderer.rasterize(rasterImage, 2 * pointA.getX() / rasterImage.width - 1, -(2 * pointA.getY() / rasterImage.height - 1), 2 * pointB.getX() / rasterImage.width - 1, -(2 * pointB.getY() / rasterImage.height - 1), color)
    }

    fun getPoint(index: Int): Point = points[index]

    fun getSize(): Int = points.size

    fun clear() = points.clear()


}
