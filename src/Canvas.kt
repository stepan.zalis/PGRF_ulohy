import models.ActualMode
import models.Circle
import models.Circle.Mode.*
import models.Lines
import rasterdata.ImagePresenterAWT
import rasterdata.RasterImageAWT
import rasterdata.interfaces.RasterImage
import java.awt.*
import java.awt.image.BufferedImage
import javax.swing.JFrame
import javax.swing.JPanel
import javax.swing.SwingUtilities
import javax.swing.WindowConstants
import rasterdata.interfaces.Presenter
import rasterizationops.LineRendererNaive
import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent
import java.util.function.Function
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent


/**
 * trida pro kresleni na platno: zobrazeni pixelu
 *
 * @author PGRF FIM UHK
 * @version 2017
 */

class Canvas(width: Int, height: Int) {

    private val frame: JFrame = JFrame()
    private val panel: JPanel
    private val img: BufferedImage

    private var lines = Lines()
    private var circle = Circle()

    private var actualMode = ActualMode.LINE
    private var rasterImage: RasterImage<Int>
    private var presenter: Presenter<Int, Graphics>
    private var lineRenderer: LineRendererNaive<Int>


    init {

        frame.layout = BorderLayout()
        frame.title = "UHK FIM PGRF : Canvas"
        frame.isResizable = false
        frame.defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE

        img = BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)

        rasterImage = RasterImageAWT<Int>(img, Function.identity(), Function.identity())
        presenter = ImagePresenterAWT()

        rasterImage.cleared(Color.WHITE.rgb)

        lineRenderer = LineRendererNaive()

        panel = object : JPanel() {

            private val serialVersionUID = 1L

            public override fun paintComponent(g: Graphics) {
                super.paintComponent(g)
                present(g)
                g.drawString("Click C to choose CIRCLE or L to choose LINE", 5, rasterImage.height - 10)
            }
        }

        panel.preferredSize = Dimension(width, height)

        panel.addMouseListener(object : MouseAdapter() {

            override fun mousePressed(e: MouseEvent) {
                if (actualMode == ActualMode.LINE && lines.getSize() == 0)
                    lines.addPoint(Point(e.x, e.y))
            }

            override fun mouseReleased(e: MouseEvent) {
                if (actualMode == ActualMode.LINE) {
                    lines.addPoint(Point(e.x, e.y))
                    draw()
                } else {
                    val mode = circle.getActualMode()
                    when (mode) {
                        CENTER -> circle.setCenter(Point(e.x, e.y))
                        else -> rasterImage = circle.draw(rasterImage, Point(e.x, e.y))
                    }
                    draw()
                    circle.setActualMode(mode)
                }
            }
        })

        panel.addMouseMotionListener(object : MouseAdapter() {

            override fun mouseDragged(e: MouseEvent) {
                if (actualMode == ActualMode.LINE) {

                    var color = Color.BLACK.rgb
                    if (lines.getSize() >= 2) color = Color.RED.rgb
                    draw()

                    val point = Point(e.x, e.y)
                    rasterImage = lines.draw(rasterImage, lines.getPoint(lines.getSize() - 1), point, color)
                    rasterImage = lines.draw(rasterImage, lines.getPoint(0), point, color)
                    panel.repaint()
                }
            }

            override fun mouseMoved(e: MouseEvent) {

                if (actualMode == ActualMode.CIRCLE && circle.getActualMode() != CENTER) {
                    circle.clearPoints()
                    redrawLines()
                    rasterImage = circle.draw(rasterImage, Point(e.x, e.y))
                    rasterImage = lines.draw(rasterImage, circle.getCenter(), Point(e.x,e.y), Color.RED.rgb)
                    panel.repaint()

                }
            }
        })

        frame.addKeyListener(object : KeyAdapter() {
            override fun keyPressed(e: KeyEvent) {
                print(e.keyChar)

                when (e.keyChar.toLowerCase()) {
                    'c' -> actualMode = ActualMode.CIRCLE
                    'l' -> actualMode = ActualMode.LINE
                }
            }
        })

        frame.add(panel, BorderLayout.CENTER)
        frame.pack()
        frame.isVisible = true
    }

    fun present(graphics: Graphics) {
        presenter.present(rasterImage, graphics)
    }

    fun draw() {
        redrawLines()
        redrewCircle()
        panel.repaint()
    }

    fun redrawLines() {
        rasterImage.cleared(Color.WHITE.rgb)
        if (lines.getSize() != 0) rasterImage = lines.drawLines(rasterImage)
        panel.repaint()
    }

    private fun redrewCircle() {
        if (circle.lastPoint != null) circle.drawLine(rasterImage)
    }

    fun start() {
        draw()
        panel.repaint()
    }

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            SwingUtilities.invokeLater { Canvas(800, 600).start() }
        }
    }

}